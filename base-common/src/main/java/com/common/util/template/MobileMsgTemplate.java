package com.common.util.template;

import lombok.Data;

import java.io.Serializable;

/**
 * 短信消息模板
 */
@Data
public class MobileMsgTemplate implements Serializable {
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 组装后的模板内容JSON字符串
     */
    private String context;
    /**
     * 短信签名
     */
    private String signName;
    /**
     * 短信模板
     */
    private String template;
    /**
     *
     */
    private String accessKeyId;
    /**
     *
     */
    private String accessKeySecret;

    public MobileMsgTemplate(String mobile, String context, String signName, String template, String accessKeyId, String accessKeySecret) {
        this.mobile = mobile;
        this.context = context;
        this.signName = signName;
        this.template = template;
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
    }
}
