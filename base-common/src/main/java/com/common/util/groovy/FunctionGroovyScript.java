package com.common.util.groovy;

import cn.hutool.core.util.StrUtil;
import groovy.lang.Script;

import java.lang.reflect.Method;

/**
 * groovy自定义函数
 */
public class FunctionGroovyScript extends Script {

    /**
     * @see groovy.lang.Script#run()
     */
    @Override
    public Object run() {
        Method[] methods = FunctionGroovyScript.class.getDeclaredMethods();
        StringBuilder sb = new StringBuilder();
        for (Method method : methods) {
            sb.append(method);
        }

        return sb.substring(0, sb.length() - 1);
    }

    /**
     * groovy字符串拼接
     * 
     * @param str               左值
     * @param val               右值
     * @return                  拼接字符串
     */
    public static Object concat(Object str, Object val) {
        return StrUtil.concat(true, str.toString(), val.toString());
    }

}
