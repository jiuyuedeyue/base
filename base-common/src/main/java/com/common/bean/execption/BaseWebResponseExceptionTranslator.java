package com.common.bean.execption;

import cn.hutool.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.stereotype.Component;

/**
 * @author yuit
 * @create 2018/11/2 9:14
 * @description
 * @modify
 */
@Component
public class BaseWebResponseExceptionTranslator implements WebResponseExceptionTranslator {

    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
        return ResponseEntity
                .status(HttpStatus.HTTP_OK)
                .body(new BaseOauthException(e.getMessage()));
    }
}
