import React, {PureComponent} from 'react';
import {Card, Icon, Input, Tree} from 'antd';
import {connect} from "dva";
import Ellipsis from '@/components/Ellipsis';

const TreeNode = Tree.TreeNode;
const Search = Input.Search;
const getParentKey = (id, tree) => {
  let parentKey;
  for (let i = 0; i < tree.length; i++) {
    const node = tree[i];
    if (node.children) {
      if (node.children.some(item => item.id === id)) {
        parentKey = node.id;
      } else if (getParentKey(id, node.children)) {
        parentKey = getParentKey(id, node.children);
      }
    }
  }
  return parentKey;
};

const dataList = [];
@connect(({sysDept, sysUser}) => ({
  sysDept,
  sysUser,
  treeData: {},
}))
export default class ListTree extends PureComponent {
  state = {
    selectedKeys: this.props.value || ['1'],
    expandedKeys: this.props.value || ['1'],
    searchValue: '',
    autoExpandParent: true,
  }

  componentWillMount() {
    this.props.dispatch({
      type: 'sysDept/getList',
      payload: {}
    });
  }

  onExpand = (expandedKeys) => {
    this.setState({
      expandedKeys,
    });
  }


  onChange = (e) => {
    const {sysDept: {list = []}} = this.props;
    const value = e.target.value;
    const expandedKeys = list.map((item) => {
      if (item.name.indexOf(value) > -1) {
        return getParentKey(item.id, list);
      }
      return null;
    }).filter((item, i, self) => item && self.indexOf(item) === i);
    this.setState({
      expandedKeys,
      searchValue: value,
      autoExpandParent: true,
    });
  }
  selectTree = (val) => {
    const deptId = val[0];
    if (!deptId) {
      return;
    }
    this.setState({selectedKeys: [deptId]})
    this.props.selectTree(deptId, this.props.treeData[deptId]);
  }

  loop = data => data.filter(item => (this.props.type || 'SUPERVISE').indexOf(item.type) === -1).map((item) => {
    const {searchValue} = this.state;
    this.props.treeData[item.id] = item;
    const index = item.name.indexOf(searchValue);
    const beforeStr = item.name.substr(0, index);
    const afterStr = item.name.substr(index + searchValue.length);
    const treeIconType = 'cluster';

    const title = index > -1 ? (
      <Ellipsis tooltip={true} lines={12}>
        {beforeStr}
        <span style={{color: '#f50'}}>{searchValue}</span>
        {afterStr}
      </Ellipsis>
    ) : <Ellipsis tooltip={true} lines={12}>{item.name}</Ellipsis>;
    if (item.children) {
      return (
        <TreeNode icon={<Icon type={treeIconType}/>} key={item.id} title={title}>
          {this.loop(item.children)}
        </TreeNode>
      );
    }
    return <TreeNode icon={<Icon type={treeIconType}/>} key={item.id} title={title}/>;
  });

  render() {
    const {expandedKeys, autoExpandParent, selectedKeys} = this.state;
    const {sysDept: {list}} = this.props;
    return (

      <Card bordered={false}>
        <Search style={{marginBottom: 8}} placeholder="请输入需要搜索的部门名称" onChange={this.onChange}/>
        <Tree
          // showIcon
          defaultExpandAll
          onExpand={this.onExpand}
          expandedKeys={expandedKeys}
          selectedKeys={selectedKeys}
          autoExpandParent={autoExpandParent}
          onSelect={this.selectTree}
        >
          {this.loop(list)}
        </Tree>
      </Card>
    );
  }
}
