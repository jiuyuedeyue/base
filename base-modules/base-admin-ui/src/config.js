module.exports = {

  // ================系统配置================

  appName: '听游运营平台',
  menuName: '听游运营平台',
  copyrightText: `${new Date().getFullYear()} 听游科技（中国）科技有限公司  版权所有`,
  logoSrc: require('./assets/logo.png'),
  basicAuth: 'YmFzZTpiYXNl',
  baseURL: 'http://127.0.0.1:8080/upms',
  // baseURL: './',
  systemIndex: '/home',
  // 公共分页信息
  pagination: {
    showSizeChanger: true,
    showQuickJumper: true,
    showTotal: (total, range) => `显示第 ${range[0]} 到第 ${range[1]} 条记录，总共 ${total} 条记录`,
    current: 1,
    total: 0,
    pageSize: 20,
    defaultPageSize: 20,
  },

  // ================系统配置================

  // ================常量================

  // 状态有效
  STATUS_VALID: 'VALID',
  // 状态无效
  STATUS_INVALID: 'INVALID',



  // ================常量================
};
