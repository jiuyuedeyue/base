import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Badge,
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  Radio,
  message,
  Modal,
  Row,
  Select,
  Table,
  Popconfirm,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Dict from '@/components/Dict';
import * as system from '../../services/upms';

import styles from './System.less';

const FormItem = Form.Item;

const statusMap = { VALID: 'success', INVALID: 'error' };

const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 14,
  },
};

@connect(({ sysDict, sysUser, loading }) => ({
  sysDict,
  sysUser,
  loading: loading.models.sysDict,
}))
@Form.create()
export default class DictList extends PureComponent {
  state = {
    modalVisible: false,
    itemType: 'create',
    item: {},
    tempSorts: {},
  };

  componentDidMount() {
    this.initQuery();
    this.props.dispatch({
      type: 'sysDict/getListType',
    });
  }

  initQuery = () => {
    const { dispatch, form } = this.props;
    dispatch({
      type: 'sysDict/getList',
      payload: form.getFieldsValue(),
    });
  };

  handleFormReset = () => {
    this.props.form.resetFields();
  };

  handleModalVisible = (flag, itemType, item) => {
    if (itemType === 'create') item = {};
    if (itemType === 'sub')
      item = {
        type: item.type,
        memo: item.memo,
      };
    if (!flag) item = {};
    this.setState({
      modalVisible: flag,
      itemType: itemType,
      item,
    });
  };

  handleAdd = fields => {
    this.props.dispatch({
      type: 'sysDict/add',
      payload: {
        ...fields,
      },
      callback: () => this.initQuery(),
    });
    this.setState({
      modalVisible: false,
    });
  };

  handleChangeSort(val, record) {
    let tempSorts = this.state.tempSorts;
    tempSorts[record.id.toString()] = { id: record.id, sort: val };
    if ((val || 0) === (record.sort || 0)) delete tempSorts[record.id];
    this.setState({ tempSorts: Object.assign({}, tempSorts) });
  }

  handleUpdateSorts() {
    let sorts = [];
    const { tempSorts } = this.state;
    Object.keys(tempSorts).map(key => {
      sorts.push(tempSorts[key]);
    });
    this.props.dispatch({
      type: 'sysDict/updateSorts',
      payload: sorts,
      callback: () => {
        this.initQuery();
        this.setState({ tempSorts: {} });
      },
    });
  }

  handleDelete(record) {
    this.props.dispatch({
      type: 'sysDict/delete',
      payload: {
        id: record.id,
      },
      callback: () => this.initQuery(),
    });
  }

  handleChangeState(record) {
    this.props.dispatch({
      type: 'sysDict/updateStatus',
      payload: {
        id: record.id,
        status: record.status === 'VALID' ? 'INVALID' : 'VALID',
      },
      callback: () => this.initQuery(),
    });
  }

  handlePageChange = page => {
    this.props.dispatch({
      type: 'sysDict/getList',
      payload: {
        current: page.current,
        size: page.pageSize,
        ...this.props.form.getFieldsValue(),
      },
    });
  };

  render() {
    const { sysDict: { list, listType, pagination }, sysUser: { currentUser }, loading, form } = this.props;
    const { modalVisible, itemType, item, tempSorts } = this.state;

    const columns = [
      {
        title: '字典标签',
        key: 'label',
        dataIndex: 'label',
      },
      {
        title: '字典值',
        key: 'value',
        dataIndex: 'value',
      },
      {
        title: '字典类型',
        key: 'type',
        dataIndex: 'type',
      },
      {
        title: '字典描述',
        key: 'memo',
        dataIndex: 'memo',
      },
      {
        title: '字典状态',
        key: 'statusDesc',
        dataIndex: 'statusDesc',
        render: (val, record) => <Badge status={statusMap[record.status]} text={val} />,
      },
      {
        title: '排序',
        key: 'sort',
        dataIndex: 'sort',
        render: (val, record) => (
          <InputNumber
            min={0}
            defaultValue={val}
            onChange={v => this.handleChangeSort(v, record)}
          />
        ),
      },
      {
        title: '操作',
        width: 150,
        render: (val, record) => (
          <Fragment>
            <a onClick={() => this.handleModalVisible(true, 'update', record)}>
              修改<Divider type="vertical" />
            </a>
            {record.status === 'VALID' ? (
              <Popconfirm
                title="禁用该字典（及其所有子字典）后将影响到界面显示，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleChangeState(record)}
              >
                <a>
                  禁用<Divider type="vertical" />
                </a>
              </Popconfirm>
            ) : (
              <Popconfirm
                title="启用该字典（及其所有子字典）后将影响到界面显示，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleChangeState(record)}
              >
                <a>
                  启用<Divider type="vertical" />
                </a>
              </Popconfirm>
            )}
            {currentUser.admin && (
              <Popconfirm
                title="删除该字典（及其所有子字典）后将影响功能正常显示且无法找回，请谨慎使用！"
                placement="topRight"
                onConfirm={() => this.handleDelete(record)}
              >
                <a>
                  删除<Divider type="vertical" />
                </a>
              </Popconfirm>
            )}
            <a onClick={() => this.handleModalVisible(true, 'sub', record)}>
              新建子字典<Divider type="vertical" />
            </a>
          </Fragment>
        ),
      },
    ];

    const createModalProps = {
      item,
      itemType,
      currentUser,
      visible: modalVisible,
      dispatch: this.props.dispatch,
      handleAdd: this.handleAdd,
      handleModalVisible: () => this.handleModalVisible(false),
    };

    const CreateFormGen = () => <CreateForm {...createModalProps} />;

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              <Form onSubmit={this.handleSearch} layout="inline">
                <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                  <Col md={6} sm={24}>
                    <FormItem>
                      {form.getFieldDecorator('type')(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="请选择类型"
                          optionFilterProp="children"
                          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                          {
                            listType && listType.map(item => {
                              return <Option value={item.type} key={item.type}>{item.type}({item.memo})</Option>;
                            })
                          }
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col md={6} sm={24}>
                    <FormItem>
                      {form.getFieldDecorator('status', {
                        initialValue: '',
                      })(<Dict type={'STATUS'} radio query />)}
                    </FormItem>
                  </Col>
                  <Col md={6} sm={24}>
                    <span className={styles.submitButtons}>
                      <Button type="primary" onClick={() => this.initQuery()}>
                        查询
                      </Button>
                      <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                        重置
                      </Button>
                    </span>
                  </Col>
                </Row>
              </Form>
            </div>
            <div className={styles.tableListOperator}>
              <Button type="primary" onClick={() => this.handleModalVisible(true, 'create')}>
                新建
              </Button>
              {Object.keys(tempSorts).length > 0 && (
                <Button type="primary" onClick={() => this.handleUpdateSorts()}>
                  更新排序
                </Button>
              )}
            </div>
            <Table
              loading={loading}
              dataSource={list}
              columns={columns}
              pagination={pagination}
              onChange={this.handlePageChange}
              rowKey={item => item.id}
            />
          </div>
        </Card>
        <CreateFormGen />
      </PageHeaderWrapper>
    );
  }
}

const CreateForm = Form.create()(props => {
  const { visible, form, itemType, item, currentUser, handleAdd, handleModalVisible } = props;

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd({
        ...fieldsValue,
        id: item.id,
        status: item.status || 'VALID',
      });
    });
  };

  const modalProps = {
    title: '新建',
    visible,
    onOk: okHandle,
    onCancel: handleModalVisible,
  };

  if (itemType === 'create') modalProps.title = '新建';
  if (itemType === 'update') modalProps.title = '修改';
  if (itemType === 'sub') modalProps.title = '新建子字典';
  return (
    <Modal {...modalProps}>
      <Form>
        <Row>
          <Col span={24}>
            <FormItem label="字典标签：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('label', {
                initialValue: item.label,
                rules: [
                  {
                    required: true,
                    message: '请输入字典标签',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="字典值：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('value', {
                initialValue: item.value,
                rules: [
                  {
                    required: true,
                    message: '请输入字典值',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="字典类型：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('type', {
                initialValue: item.type,
                rules: [
                  {
                    required: true,
                    message: '请输入字典类型',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="字典描述：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('memo', {
                initialValue: item.memo,
              })(<Input />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem label="字典排序：" hasFeedback {...formItemLayout}>
              {form.getFieldDecorator('sort', {
                initialValue: item.sort,
                rules: [
                  {
                    required: true,
                    message: '请输入字典顺序',
                  },
                ],
              })(<InputNumber min={0} style={{ width: '100%' }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
});
