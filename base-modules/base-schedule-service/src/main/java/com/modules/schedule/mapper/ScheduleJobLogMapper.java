package com.modules.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.modules.schedule.entity.ScheduleJobLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务日志Dao
 *
 * @date 2019-01-22 10:13:48
 */
@Mapper
public interface ScheduleJobLogMapper extends BaseMapper<ScheduleJobLog>, com.common.mapper.BaseMapper<ScheduleJobLog> {

}
