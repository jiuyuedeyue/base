package com.modules.schedule.service.impl;

import com.common.service.BaseServiceImpl;
import com.modules.schedule.entity.ScheduleJobLog;
import com.modules.schedule.mapper.ScheduleJobLogMapper;
import com.modules.schedule.service.ScheduleJobLogService;
import org.springframework.stereotype.Service;

/**
 */
@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl extends BaseServiceImpl<ScheduleJobLogMapper, ScheduleJobLog> implements ScheduleJobLogService {


}
