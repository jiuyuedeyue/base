package com.modules.schedule.service;

import com.common.service.BaseService;
import com.modules.schedule.entity.ScheduleJob;

/**
 * 定时任务
 *
 */
public interface ScheduleJobService extends BaseService<ScheduleJob> {


    /**
     * 批量删除定时任务
     *
     * @param jobIds jobIds
     */
    void deleteBatch(String[] jobIds);

    /**
     * 批量更新定时任务状态
     *
     * @param jobIds jobIds
     * @param status status
     */
    void updateBatch(String[] jobIds, String status);

    /**
     * 立即执行
     *
     * @param jobIds jobIds
     */
    void run(String[] jobIds);

    /**
     * 暂停运行
     *
     * @param jobIds jobIds
     */
    void pause(String[] jobIds);

    /**
     * 恢复运行
     *
     * @param jobIds jobIds
     */
    void resume(String[] jobIds);
}
