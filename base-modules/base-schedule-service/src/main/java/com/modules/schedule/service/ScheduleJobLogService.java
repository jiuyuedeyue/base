package com.modules.schedule.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.service.BaseService;
import com.modules.schedule.entity.ScheduleJobLog;

import java.util.Map;

/**
 * 定时任务日志
 *
 */
public interface ScheduleJobLogService extends BaseService<ScheduleJobLog> {

}
