package com.modules.schedule.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.util.R;
import com.modules.schedule.entity.ScheduleJobLog;
import com.modules.schedule.service.ScheduleJobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 定时任务日志
 *
 */
@RestController
@RequestMapping("/schedule/scheduleLog")
public class ScheduleJobLogController {
    @Autowired
    private ScheduleJobLogService scheduleJobLogService;

    /**
     * 分页查询定时任务
     *
     * @param scheduleJobLog 查询参数
     * @return R
     */
    @GetMapping("/listPage")
    public R listPage(@ModelAttribute Page page, @ModelAttribute ScheduleJobLog scheduleJobLog) {
        return R.success(scheduleJobLogService.getPage(page, scheduleJobLog));
    }

}
