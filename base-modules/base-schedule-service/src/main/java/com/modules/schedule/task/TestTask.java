package com.modules.schedule.task;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 测试定时任务(演示Demo，可删除)
 * 
 */
@Component("testTask")
public class TestTask {

	private Logger logger = LoggerFactory.getLogger(getClass());


    /**
     * 任务执行类
     * @throws Exception
     */
	public void execute() throws Exception {
        logger.info(DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));
	}
}
