package com.modules.schedule.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.common.enums.CommonEnum;
import com.common.service.BaseServiceImpl;
import com.modules.schedule.entity.ScheduleJob;
import com.modules.schedule.mapper.ScheduleJobMapper;
import com.modules.schedule.service.ScheduleJobService;
import com.modules.schedule.utils.ScheduleUtils;
import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Service("scheduleJobService")
public class ScheduleJobServiceImpl extends BaseServiceImpl<ScheduleJobMapper, ScheduleJob> implements ScheduleJobService {
    @Autowired
    private Scheduler scheduler;

    @Value("quartz.taskGroup")
    private String taskGroup;

    /**
     * 项目启动时，初始化定时器
     */
    @PostConstruct
    public void init() {
        ScheduleJob queryJob = new ScheduleJob();
        queryJob.setTaskGroup(taskGroup);
        List<ScheduleJob> scheduleJobList = this.getList(queryJob);
        for (ScheduleJob scheduleJob : scheduleJobList) {
            CronTrigger cronTrigger = ScheduleUtils.getCronTrigger(scheduler, scheduleJob.getTaskGroup(), scheduleJob.getId().toString());
            //如果不存在，则创建
            if (cronTrigger == null) {
                ScheduleUtils.createScheduleJob(scheduler, scheduleJob);
            } else {
                ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
            }
        }
    }

    @Override
    public boolean saveOrUpdate(ScheduleJob scheduleJob) {
        boolean flag = false;
        if (scheduleJob.getIsNewRecord()) {
            flag = save(scheduleJob);
            ScheduleUtils.createScheduleJob(scheduler, scheduleJob);
        } else {
            flag = updateById(scheduleJob);
            ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
        }
        return flag;
    }

    @Override
    public void deleteBatch(String[] jobIds) {
        for (String jobId : jobIds) {
            ScheduleUtils.deleteScheduleJob(scheduler, jobId);
        }

        //删除数据
        this.deleteByIds(CollUtil.newArrayList(jobIds));
    }

    @Override
    public void updateBatch(String[] jobIds, String status) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("list", jobIds);
        map.put("status", status);
        baseMapper.updateBatch(map);
    }

    @Override
    public void run(String[] jobIds) {
        for (String jobId : jobIds) {
            ScheduleUtils.run(scheduler, this.getById(jobId));
        }
    }

    @Override
    public void pause(String[] jobIds) {
        for (String jobId : jobIds) {
            ScheduleUtils.pauseJob(scheduler, jobId);
        }

        updateBatch(jobIds, CommonEnum.INVALID.getCode());
    }

    @Override
    public void resume(String[] jobIds) {
        for (String jobId : jobIds) {
            ScheduleUtils.resumeJob(scheduler, jobId);
        }

        updateBatch(jobIds, CommonEnum.VALID.getCode());
    }

}
