package com.modules.schedule.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.util.R;
import com.modules.schedule.entity.ScheduleJob;
import com.modules.schedule.service.ScheduleJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * 定时任务
 *
 */
@RestController
@RequestMapping("/schedule/scheduleJob")
public class ScheduleJobController {

    @Autowired
    private ScheduleJobService scheduleJobService;

    @Value("quartz.taskGroup")
    private String taskGroup;

    /**
     * 分页查询定时任务
     *
     * @param scheduleJob 查询参数
     * @return R
     */
    @GetMapping("/listPage")
    public R listPage(@ModelAttribute Page page, @ModelAttribute ScheduleJob scheduleJob) {
        scheduleJob.setTaskGroup(taskGroup);
        return R.success(scheduleJobService.getPage(page, scheduleJob));
    }

    /**
     * 新增定时任务
     *
     * @param scheduleJob scheduleJob
     * @return R
     */
    @PostMapping("/save")
    public R save(@RequestBody ScheduleJob scheduleJob) {
        scheduleJob.setTaskGroup(taskGroup);
        scheduleJobService.saveOrUpdate(scheduleJob);
        return R.success();
    }

    /**
     * 删除定时任务
     *
     * @param jobIds jobIds
     * @return R
     */
    @PostMapping("/delete")
    public R delete(@RequestBody String[] jobIds) {
        scheduleJobService.deleteBatch(jobIds);

        return R.success();
    }

    /**
     * 立即执行任务
     *
     * @param jobIds jobIds
     * @return R
     */
    @PostMapping("/run")
    public R run(@RequestBody String[] jobIds) {
        scheduleJobService.run(jobIds);

        return R.success();
    }

    /**
     * 暂停定时任务
     *
     * @param jobIds jobIds
     * @return R
     */
    @PostMapping("/pause")
    public R pause(@RequestBody String[] jobIds) {
        scheduleJobService.pause(jobIds);

        return R.success();
    }

    /**
     * 恢复定时任务
     *
     * @param jobIds jobIds
     * @return R
     */
    @PostMapping("/resume")
    public R resume(@RequestBody String[] jobIds) {
        scheduleJobService.resume(jobIds);

        return R.success();
    }
}
