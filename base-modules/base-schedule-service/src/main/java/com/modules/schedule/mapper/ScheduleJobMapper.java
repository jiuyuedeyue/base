package com.modules.schedule.mapper;

import com.common.mapper.BaseMapper;
import com.modules.schedule.entity.ScheduleJob;

import java.util.Map;

/**
 * 定时任务Dao
 *
 * @date 2019-01-22 10:13:48
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

    /**
     * 批量更新状态
     *
     * @param map map
     * @return int
     */
    int updateBatch(Map<String, Object> map);

}
