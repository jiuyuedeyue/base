package com.modules.upms.service;

import com.modules.upms.entity.SysRegion;
import com.common.service.BaseService;

/**
 * <p>
 * 行政区域 服务类
 * </p>
 *
 * @author xmh
 */
public interface SysRegionService extends BaseService<SysRegion> {

}
