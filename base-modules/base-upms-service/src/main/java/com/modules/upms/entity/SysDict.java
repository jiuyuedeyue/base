package com.modules.upms.entity;

import com.common.entity.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 字典表
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysDict extends DataEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 数据值
     */
    private String value;
    /**
     * 标签名
     */
    private String label;
    /**
     * 类型
     */
    private String type;
    /**
     * 状态
     */
    private String status;
    /**
     * 状态
     */
    private String statusDesc;
    /**
     * 排序（升序）
     */
    private Integer sort;

}