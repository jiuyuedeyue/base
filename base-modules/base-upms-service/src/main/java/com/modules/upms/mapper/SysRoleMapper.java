package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysRole;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 通过部门ID查询角色列表
     *
     * @param deptId 部门ID
     * @return 角色列表
     */
    List<SysRole> selectListByDeptId(Integer deptId);

    /**
     * 查询用户角色列表
     *
     * @param userId
     * @return
     */
    List<SysRole> findListByUserId(Long userId);

    /**
     * 角色编码是否唯一
     * @param role
     * @return
     */
    int isRoleCodeExists(SysRole role);

    /**
     * 删除角色菜单
     * @param role
     */
    void deleteRoleMenu(SysRole role);

    /**
     * 插入角色菜单
     * @param role
     */
    void insertRoleMenu(SysRole role);

    /**
     * 分配用户
     * @param role
     */
    void saveAssignUser(SysRole role);

    /**
     * 删除角色用户
     * @param role
     */
    void deleteRoleUser(SysRole role);

    /**
     * 删除角色部门
     * @param role
     */
    void deleteRoleDept(SysRole role);

    /**
     * 插入角色部门
     * @param role
     */
    void insertRoleDept(SysRole role);

    /**
     * 根据角色查询数据范围为明细的部门数据
     * @param roleId
     */
    List<Long> getScopeDetailByRoleId(Long roleId);
}