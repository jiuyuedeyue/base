package com.modules.upms.service.impl;

import com.modules.upms.entity.SysAttachmentInfo;
import com.modules.upms.mapper.SysAttachmentInfoMapper;
import com.modules.upms.service.SysAttachmentInfoService;
import com.common.service.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统附件表 服务实现类
 * </p>
 *
 * @author xmh
 */
@Service
public class SysAttachmentInfoServiceImpl extends BaseServiceImpl<SysAttachmentInfoMapper, SysAttachmentInfo>implements SysAttachmentInfoService {

    /**
     * 系统附件表 Mapper 接口
     */
    @Autowired
    private SysAttachmentInfoMapper sysAttachmentInfoMapper;
}
