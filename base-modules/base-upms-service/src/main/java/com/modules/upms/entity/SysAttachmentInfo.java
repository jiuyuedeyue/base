package com.modules.upms.entity;

import com.common.entity.DataEntity;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * <p>
 * 系统附件表
 * </p>
 *
 * @author xmh
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysAttachmentInfo extends DataEntity {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(notes = "业务ID")
    private String bizId;


    @ApiModelProperty(notes = "业务类型")
    private String bizType;


    @ApiModelProperty(notes = "文件名称")
    private String fileName;


    @ApiModelProperty(notes = "文件路径")
    private String filePath;







}
