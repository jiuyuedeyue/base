package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.entity.SysLog;

/**
 * <p>
  * 日志表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2017-11-20
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}