package com.modules.upms.mapper;

import com.modules.upms.entity.SysAttachmentInfo;
import com.common.mapper.BaseMapper;

/**
 * <p>
 * 系统附件表 Mapper 接口
 * </p>
 *
 * @author xmh
 */
public interface SysAttachmentInfoMapper extends BaseMapper<SysAttachmentInfo> {

}
