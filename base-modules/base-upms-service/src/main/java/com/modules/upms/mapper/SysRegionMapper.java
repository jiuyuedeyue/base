package com.modules.upms.mapper;

import com.modules.upms.entity.SysRegion;
import com.common.mapper.BaseMapper;

/**
 * <p>
 * 行政区域 Mapper 接口
 * </p>
 *
 * @author xmh
 */
public interface SysRegionMapper extends BaseMapper<SysRegion> {

}
