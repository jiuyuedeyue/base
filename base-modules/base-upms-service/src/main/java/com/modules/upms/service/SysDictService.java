package com.modules.upms.service;

import com.common.service.BaseService;
import com.modules.upms.entity.SysDict;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 */
public interface SysDictService extends BaseService<SysDict> {

    /**
     * 获取指定编码下的所有子字典
     * @param type
     * @return
     */
    List<SysDict> getListByType(String type);

    /**
     * 更新字典排序
     * @param list
     */
    void updateSorts(List<SysDict> list);

    /**
     * 获取所有的类型
     * @return
     */
    List<SysDict> getListType();

    /**
     * 更新状态
     * @param sysDict
     */
    void updateStatus(SysDict sysDict);
}
