package com.modules.upms.entity;

import com.common.entity.DataEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户表
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysUser extends DataEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 部门ID
     */
    private Long deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 角色id
     */
    private String roleId;
    /**
     * 角色ids
     */
    private String roleIds;
    /**
     * 用户名
     */
    private String loginName;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 状态
     */
    private String status;
    /**
     * 状态描述
     */
    private String statusDesc;
    /**
     * 随机盐
     */
    @JsonIgnore
    private String salt;
    /**
     * 类型
     */
    private String type;
    /**
     * 类型描述
     */
    private String typeDesc;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 角色列表
     */
    private List<SysRole> roleList;
    /**
     * 功能列表
     */
    private List<SysMenu> menuList;
    /**
     * 部门ID列表
     */
    private List<Long> deptIdList;

    /**
     * 最后一次重置密码的时间
     */
    private Date lastPasswordResetDate;

}
