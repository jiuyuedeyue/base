package com.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysDept;
import com.modules.upms.mapper.SysDeptMapper;
import com.modules.upms.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 部门管理 服务实现类
 * </p>
 */
@Service
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Override
    public boolean saveOrUpdate(SysDept entity) {
        return super.saveOrUpdate(entity);
    }

    @Override
    public void updateSorts(List<SysDept> list) {
        if(CollUtil.isNotEmpty(list)) {
            list.forEach(sysDept -> sysDeptMapper.updateSorts(sysDept));
        }
    }
}
