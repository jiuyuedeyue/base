package com.modules.upms.service.impl;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.constant.SecurityConstants;
import com.common.service.BaseServiceImpl;
import com.common.util.R;
import com.common.util.template.MobileMsgTemplate;
import com.modules.upms.entity.SysMenu;
import com.modules.upms.entity.SysRole;
import com.modules.upms.entity.SysUser;
import com.modules.upms.mapper.SysDeptMapper;
import com.modules.upms.mapper.SysMenuMapper;
import com.modules.upms.mapper.SysRoleMapper;
import com.modules.upms.mapper.SysUserMapper;
import com.modules.upms.service.SysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Override
    @Cacheable(value = "user_details", key = "#loginName")
    public SysUser findUserByLoginName(String loginName) {
        SysUser user = sysUserMapper.selectUserByLoginName(loginName);
        if (user != null) {
            List<SysRole> roleList = sysRoleMapper.findListByUserId(user.getId());
            user.setRoleList(roleList);
            List<SysMenu> menuList = sysMenuMapper.findMenuByUserId(user.getId());
            user.setMenuList(menuList);

            Set<Long> deptIdList = CollUtil.newHashSet();
            for (SysRole sysRole : roleList) {
                //所在机构及以下
                if ("SCOPE_DEPT_ALL".equals(sysRole.getDataScope())) {
                    List<Long> deptIdListTemp = sysDeptMapper.getScopeDeptAllByRoleId(user.getDeptId());
                    deptIdList.addAll(deptIdListTemp);
                }
                //所在机构
                else if ("SCOPE_DEPT".equals(sysRole.getDataScope())) {
                    deptIdList.add(user.getDeptId());
                }
                //明细
                else if ("SCOPE_DETAIL".equals(sysRole.getDataScope())) {
                    List<Long> deptIdListTemp = sysRoleMapper.getScopeDetailByRoleId(sysRole.getId());
                    deptIdList.addAll(deptIdListTemp);
                }
            }
            user.setDeptIdList(CollUtil.newArrayList(deptIdList));
        }
        return user;
    }

    /**
     * 通过手机号查询用户信息
     *
     * @param mobile 手机号
     * @return 用户信息
     */
    @Override
    @Cacheable(value = "user_details_mobile", key = "#mobile")
    public SysUser findUserByMobile(String mobile) {
        SysUser user = sysUserMapper.selectUserByMobile(mobile);
        if (user != null) {
            List<SysRole> roleList = sysRoleMapper.findListByUserId(user.getId());
            user.setRoleList(roleList);
        }
        return user;
    }

    /**
     * 通过openId查询用户
     *
     * @param openId openId
     * @return 用户信息
     */
    @Override
    @Cacheable(value = "user_details_openid", key = "#openId")
    public SysUser findUserByOpenId(String openId) {
        SysUser user = sysUserMapper.selectUserByOpenId(openId);
        if (user != null) {
            List<SysRole> roleList = sysRoleMapper.findListByUserId(user.getId());
            user.setRoleList(roleList);
        }
        return user;
    }

    @Override
    public Boolean isExists(SysUser sysUser) {
        return sysUserMapper.isExists(sysUser) >= 1;
    }

    @Override
    public List<SysUser> getUsersByRoleId(SysUser sysUser) {
        return sysUserMapper.getUsersByRoleId(sysUser);
    }

    @Override
    public boolean saveOrUpdate(SysUser entity) {
        boolean result = super.saveOrUpdate(entity);
        if (CollUtil.isNotEmpty(entity.getRoleList())) {
            sysUserMapper.deleteUserRoleByUserId(entity.getId());
            sysUserMapper.saveBatchUserRole(entity);
        }
        return result;
    }

    @Override
    public void updateStatus(SysUser sysUser) {
        sysUserMapper.updateStatus(sysUser);
    }

    @Override
    public void updateUserPasswordById(Long id, String password, Date lastPasswordResetDate) {
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        sysUser.setPassword(password);
        sysUser.setLastPasswordResetDate(lastPasswordResetDate);
        sysUserMapper.updateUserPasswordById(sysUser);
    }

    /**
     * 保存用户验证码，和randomStr绑定
     *
     * @param randomStr 客户端生成
     * @param imageCode 验证码信息
     */
    @Override
    public void saveImageCode(String randomStr, String imageCode) {
        //redisTemplate.opsForValue().set(SecurityConstants.DEFAULT_CODE_KEY + randomStr, imageCode, SecurityConstants.DEFAULT_IMAGE_EXPIRE, TimeUnit.SECONDS);
    }

    @Override
    public boolean deleteById(Serializable id) {
        sysUserMapper.deleteUserRoleByUserId(Convert.toLong(id.toString()));
        return super.deleteById(id);
    }
}
