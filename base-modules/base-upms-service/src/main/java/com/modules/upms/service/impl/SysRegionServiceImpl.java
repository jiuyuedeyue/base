package com.modules.upms.service.impl;

import com.modules.upms.entity.SysRegion;
import com.modules.upms.mapper.SysRegionMapper;
import com.modules.upms.service.SysRegionService;
import com.common.service.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 行政区域 服务实现类
 * </p>
 *
 * @author xmh
 */
@Service
public class SysRegionServiceImpl extends BaseServiceImpl<SysRegionMapper, SysRegion>implements SysRegionService {

    /**
     * 行政区域 Mapper 接口
     */
    @Autowired
    private SysRegionMapper sysRegionMapper;
}
