package com.modules.upms.controller;

import com.common.util.R;
import com.common.util.UserUtils;
import com.common.web.BaseController;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录登出
 */
@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController extends BaseController {

    @Autowired
    @Qualifier("consumerTokenServices")
    private ConsumerTokenServices consumerTokenServices;

    /**
     * 退出登录
     *
     * @return true/false
     */
    @GetMapping(value = "/logout")
    public R<Boolean> logout(String token) {
        return R.success( consumerTokenServices.revokeToken(token));
    }
}