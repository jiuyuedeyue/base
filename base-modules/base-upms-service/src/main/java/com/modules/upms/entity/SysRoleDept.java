package com.modules.upms.entity;

import com.common.entity.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 角色与部门对应关系
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysRoleDept extends DataEntity {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
	private Integer roleId;
    /**
     * 部门ID
     */
	private Integer deptId;

}
