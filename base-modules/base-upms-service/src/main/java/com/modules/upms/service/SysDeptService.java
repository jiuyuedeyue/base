package com.modules.upms.service;

import com.common.service.BaseService;
import com.modules.upms.entity.SysDept;

import java.util.List;

/**
 * <p>
 * 部门管理 服务类
 * </p>
 */
public interface SysDeptService extends BaseService<SysDept> {

    /**
     * 更新部门排序
     * @param list
     */
    void updateSorts(List<SysDept> list);
}
