package com.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysDict;
import com.modules.upms.mapper.SysDictMapper;
import com.modules.upms.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 */
@Service
public class SysDictServiceImpl extends BaseServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    @Autowired
    private SysDictMapper sysDictMapper;

    @Override
    public List<SysDict> getListByType(String type) {
        return sysDictMapper.getListByType(type);
    }

    @Override
    public void updateSorts(List<SysDict> list) {
        if (CollUtil.isNotEmpty(list)) {
            list.forEach(sysDict -> sysDictMapper.updateSort(sysDict));
        }
    }

    @Override
    public void updateStatus(SysDict sysDict) {
        sysDictMapper.updateStatus(sysDict);
    }

    @Override
    public List<SysDict> getListType() {
        return sysDictMapper.getListType();
    }
}
