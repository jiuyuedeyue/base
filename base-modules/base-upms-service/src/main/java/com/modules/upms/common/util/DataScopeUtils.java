package com.modules.upms.common.util;

import com.modules.upms.annotation.DataScopeAop;
import org.apache.ibatis.mapping.MappedStatement;

import java.lang.reflect.Method;

/**
 * 自定义权限相关工具类
 */
public class DataScopeUtils {

    /**
     * 根据 StatementHandler 获取 注解对象
     */
    public static DataScopeAop getDataScopeByDelegate(MappedStatement mappedStatement){
        DataScopeAop permissionAop = null;
        try {
            String id = mappedStatement.getId();
            String className = id.substring(0, id.lastIndexOf("."));
            String methodName = id.substring(id.lastIndexOf(".") + 1, id.length());
            final Class cls = Class.forName(className);
            final Method[] method = cls.getMethods();
            for (Method me : method) {
                if (me.getName().equals(methodName) && me.isAnnotationPresent(DataScopeAop.class)) {
                    permissionAop = me.getAnnotation(DataScopeAop.class);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return permissionAop;
    }
}