package com.modules.upms.service.impl;

import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysLog;
import com.modules.upms.mapper.SysLogMapper;
import com.modules.upms.service.SysLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2017-11-20
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
