package com.modules.upms.entity;

import com.common.entity.DataEntity;

import com.common.entity.TreeNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * <p>
 * 行政区域
 * </p>
 *
 * @author xmh
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysRegion extends TreeNode {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(notes = "父ID")
    private Long parentId;


    @ApiModelProperty(notes = "编码")
    private String code;


    @ApiModelProperty(notes = "名称")
    private String name;


    @ApiModelProperty(notes = "英语名称")
    private String nameEn;


    @ApiModelProperty(notes = "简称")
    private String shortNameEn;


}
