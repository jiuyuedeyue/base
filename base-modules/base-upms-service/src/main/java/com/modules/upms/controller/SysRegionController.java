package com.modules.upms.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.util.R;
import com.common.web.BaseController;
import com.modules.upms.common.util.TreeUtil;
import com.modules.upms.entity.SysRegion;
import com.modules.upms.service.SysRegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 行政区域 前端控制器
 * </p>
 *
 * @author xmh
 * @since 2019-01-16
 */
@Api(tags = {"SysRegion" }, description = "行政区域相关接口")
@RestController
@RequestMapping(value = "/sys/region")
public class SysRegionController extends BaseController {

    /**
     * 行政区域服务类
     */
    @Autowired
    private SysRegionService sysRegionService;

    /**
     * 返回行政区域列表
     *
     * @return 行政区域列表
     */
    @ApiOperation(value = "行政区域列表", response = SysRegion.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "编码", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "nameEn", value = "英语名称", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "shortNameEn", value = "简称", required = false, dataType = "String", paramType = "query")
    })
    @GetMapping(value = "/listTree")
    public R<List<SysRegion>> listTree(@ModelAttribute SysRegion query){
        List<SysRegion> list = sysRegionService.getList(query);
        return R.success(TreeUtil.bulid(list, 1));
    }

    /**
     * 返回行政区域列表
     *
     * @return 行政区域列表
     */
    @ApiOperation(value = "行政区域列表", response = SysRegion.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "第几页", required = true, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页行数", required = true, dataType = "int", paramType = "query", defaultValue = "20"),
            @ApiImplicitParam(name = "code", value = "编码", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "nameEn", value = "英语名称", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "shortNameEn", value = "简称", required = false, dataType = "String", paramType = "query")
    })
    @GetMapping(value = "/listPage")
    public R<IPage<SysRegion>> listPage(@ModelAttribute Page page, @ModelAttribute SysRegion query){
        return R.success(sysRegionService.getPage(page, query));
    }

    /**
     * 保存行政区域
     *
     * @param sysRegion 实体
     * @return success/false
     */
    @PostMapping(value = "/save")
    @ApiOperation(value = "行政区域保存")
    public R<Boolean> save(@RequestBody SysRegion sysRegion){
        return R.success(sysRegionService.saveOrUpdate(sysRegion));
    }

    /**
     * 删除行政区域
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "根据ID删除行政区域")
    public R<Boolean> delete(@PathVariable Long id){
        return R.success(sysRegionService.deleteById(id));
    }

}
