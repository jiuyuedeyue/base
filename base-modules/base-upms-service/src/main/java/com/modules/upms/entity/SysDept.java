package com.modules.upms.entity;

import com.common.entity.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 部门管理
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysDept extends TreeNode {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;
    /**
     * 名称拼音
     */
    private String namePy;
    /**
     * 编码
     */
    private String code;
    /**
     * 排序号
     */
    private Integer sort;
    /**
     * 状态
     */
    private String status;
    /**
     * 状态描述
     */
    private String statusDesc;
    /**
     * 类型
     */
    private String type;
    /**
     * 类型描述
     */
    private String typeDesc;
    /**
     * 级别
     */
    private String level;
    /**
     * 级别描述
     */
    private String levelDesc;
    /**
     * 地址
     */
    private String address;
    /**
     * 职能
     */
    private String functional;
    /**
     * 负责人
     */
    private String leader;
    /**
     * 负责人手机号码
     */
    private String leaderMobile;

}
