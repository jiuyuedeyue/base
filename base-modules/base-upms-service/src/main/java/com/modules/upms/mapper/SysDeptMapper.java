package com.modules.upms.mapper;

import com.common.mapper.BaseMapper;
import com.modules.upms.annotation.DataScopeAop;
import com.modules.upms.entity.SysDept;

import java.util.List;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {


    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     *
     * @param entity 实体对象封装操作类（可以为 null）
     */
    @DataScopeAop("id")
    List<SysDept> getList(SysDept entity);

    /**
     * 更新排序
     * @param sysDept
     */
    void updateSorts(SysDept sysDept);

    /**
     * 根据角色查询数据范围为所在部门及以下的部门数据
     * @param id
     * @return
     */
    List<Long> getScopeDeptAllByRoleId(Long id);
}