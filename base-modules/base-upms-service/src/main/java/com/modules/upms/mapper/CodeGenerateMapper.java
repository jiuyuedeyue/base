package com.modules.upms.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成 Mapper 接口
 * </p>
 */
public interface CodeGenerateMapper {

    /**
     * 获取所有的表结构
     */
    List<Map<String, Object>> getTableList(@Param("database") String database);

    /**
     * 获取所有的字段结构
     */
    List<Map<String, Object>> getTableColumnList(@Param("database") String database, @Param("tableName") String tableName);
}