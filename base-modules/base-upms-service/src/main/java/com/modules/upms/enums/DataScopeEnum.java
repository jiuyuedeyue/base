package com.modules.upms.enums;

import cn.hutool.core.util.StrUtil;

/**
 * 数据权限范围枚举
 */
public enum DataScopeEnum {

    /**
     * 所有数据
     */
    SCOPE_ALL("SCOPE_ALL", "所有数据"),

    /**
     * 所在部门及以下数据
     */
    SCOPE_DEPT_ALL("SCOPE_DEPT_ALL", "所在部门及以下数据"),

    /**
     * 所在部门数据
     */
    SCOPE_DEPT("SCOPE_DEPT", "所在部门数据"),

    /**
     * 仅本人数据
     */
    SCOPE_SELF("SCOPE_SELF", "仅本人数据"),

    /**
     * 按明细设置
     */
    SCOPE_DETAIL("SCOPE_DETAIL", "按明细设置");

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    DataScopeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 根据编码获取
     * @param code
     * @return
     */
    public static DataScopeEnum getByCode(String code) {
        for (DataScopeEnum commonSEnum : DataScopeEnum.values()) {
            if (StrUtil.equals(commonSEnum.getCode(), code)) {
                return commonSEnum;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
