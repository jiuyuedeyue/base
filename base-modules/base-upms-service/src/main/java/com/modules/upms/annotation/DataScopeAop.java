package com.modules.upms.annotation;

import java.lang.annotation.*;

/**
 * 数据权限过滤
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScopeAop {

    /**
     * 限制范围的字段名称
     * @return
     */
    String value() default "dept_id";

}