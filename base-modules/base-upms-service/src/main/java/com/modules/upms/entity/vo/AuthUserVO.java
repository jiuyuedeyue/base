package com.modules.upms.entity.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.common.constant.SecurityConstants;
import com.common.enums.CommonEnum;
import com.modules.upms.entity.SysMenu;
import com.modules.upms.entity.SysRole;
import com.modules.upms.entity.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 */
public class AuthUserVO implements UserDetails {
    private static final long serialVersionUID = 1L;
    private boolean isAdmin;
    private Long userId;
    private String userName;
    private String userType;
    private String password;
    private String status;
    private Date lastPasswordResetDate;
    private List<SysRole> roleList;
    private List<SysMenu> menuList;
    private List<Long> deptIdList;

    public AuthUserVO(SysUser userVo) {
        this.userId = userVo.getId();
        this.userName = userVo.getUserName();
        this.userType = userVo.getType();
        this.password = userVo.getPassword();
        this.status = userVo.getStatus();
        this.lastPasswordResetDate = userVo.getLastPasswordResetDate();
        roleList = userVo.getRoleList();
        deptIdList = userVo.getDeptIdList();
        menuList = userVo.getMenuList();
    }

    public AuthUserVO(Long userId, String userName, String userType, String status) {
        this.userId = userId;
        this.userType = userType;
        this.userName = userName;
        this.status = status;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (SysRole role : roleList) {
            authorityList.add(new SimpleGrantedAuthority(role.getCode()));
        }
        if(CollUtil.isNotEmpty(menuList)) {
            for (SysMenu sysMenu : menuList) {
                if(StrUtil.equals(sysMenu.getDelFlag(), CommonEnum.DEL_FLAG_DEL.getCode())) {
                    continue;
                }
                if(StrUtil.equals(sysMenu.getStatus(), CommonEnum.INVALID.getCode())) {
                    continue;
                }
                if(StrUtil.isBlank(sysMenu.getPermission())) {
                    continue;
                }
                authorityList.add(new SimpleGrantedAuthority(sysMenu.getPermission()));
            }
        }
        authorityList.add(new SimpleGrantedAuthority(SecurityConstants.BASE_ROLE));
        return authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !StrUtil.equals("LOCK", status);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return StrUtil.equals("VALID", status);
    }

    public boolean isAdmin() {
        return this.userId == 1L;
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public String getStatus() {
        return status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<Long> getDeptIdList() {
        return deptIdList;
    }

    public void setDeptIdList(List<Long> deptIdList) {
        this.deptIdList = deptIdList;
    }

    public List<SysMenu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<SysMenu> menuList) {
        this.menuList = menuList;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }
}
