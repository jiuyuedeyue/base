package com.modules.upms.controller;

import com.common.util.R;
import com.common.web.BaseController;
import com.modules.upms.entity.SysRole;
import com.modules.upms.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lengleng
 * @date 2017/11/5
 */
@RestController
@RequestMapping(value = "/sys/role")
public class SysRoleController extends BaseController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 验证编码是否唯一
     * @param role
     * @return
     */
    @GetMapping(value = "/isCodeExists")
    public R isCodeExists(SysRole role) {
        return R.success(sysRoleService.isRoleCodeExists(role));
    }

    /**
     * 获取角色列表
     *
     * @param deptId 部门ID
     * @return 角色列表
     */
    @GetMapping(value = "/roleList/{deptId}")
    public List<SysRole> roleList(@PathVariable Integer deptId) {
        return sysRoleService.selectListByDeptId(deptId);

    }

    /**
     * 分页查询角色信息
     *
     * @param sysRole 查询对象
     * @return 分页对象
     */
    @GetMapping(value = "/list")
    public R<List<SysRole>> list(SysRole sysRole) {
        return R.success(sysRoleService.getList(sysRole));
    }

    /**
     * 添加角色
     *
     * @param sysRole 角色信息
     * @return success、false
     */
    @PostMapping(value = "/save")
    public R<Boolean> save(@RequestBody SysRole sysRole) {
        return new R<>(sysRoleService.saveOrUpdate(sysRole));
    }


    /**
     * 保存角色功能权限
     * @param role
     * @return
     */
    @PostMapping(value = "/saveMenuAuth")
    public R saveMenuAuth(@RequestBody SysRole role) {
        sysRoleService.saveRoleMenuAuth(role);
        return R.success();
    }

    /**
     * 分配角色用户
     * @param role
     * @return
     */
    @PostMapping(value = "/assignUser")
    public R assignUser(@RequestBody SysRole role) {
        sysRoleService.saveAssignUser(role);
        return R.success();
    }

    /**
     * 删除指定角色下的指定用户
     * @param role
     * @return
     */
    @PostMapping(value = "/deleteRoleUser")
    public R deleteRoleUser(@RequestBody SysRole role) {
        sysRoleService.deleteRoleUser(role);
        return R.success();
    }

    /**
     * 删除角色
     *
     * @param id 角色ID
     * @return success/false
     * TODO  级联删除下级节点
     */
    @DeleteMapping(value = "/{id}")
    public R<Boolean> deleteById(@PathVariable Long id) {
        return new R<>(sysRoleService.deleteById(id));
    }
}
