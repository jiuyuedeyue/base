package com.modules.upms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.common.service.BaseServiceImpl;
import com.modules.upms.entity.SysRole;
import com.modules.upms.enums.DataScopeEnum;
import com.modules.upms.mapper.SysDeptMapper;
import com.modules.upms.mapper.SysRoleMapper;
import com.modules.upms.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 通过部门ID查询角色列表
     *
     * @param deptId 部门ID
     * @return 角色列表
     */
    @Override
    public List<SysRole> selectListByDeptId(Integer deptId) {
        return sysRoleMapper.selectListByDeptId(deptId);
    }

    @Override
    public Boolean isRoleCodeExists(SysRole role) {
        return sysRoleMapper.isRoleCodeExists(role) >= 1;
    }

    @Override
    public boolean saveOrUpdate(SysRole role) {
        boolean res = super.saveOrUpdate(role);
        return res;
    }

    @Override
    public void saveRoleMenuAuth(SysRole role) {
        super.saveOrUpdate(role);
        sysRoleMapper.deleteRoleMenu(role);
        sysRoleMapper.deleteRoleDept(role);
        if(CollUtil.isNotEmpty(role.getMenuList())) {
            sysRoleMapper.insertRoleMenu(role);
        }
        if(CollUtil.isNotEmpty(role.getDeptList())) {
            sysRoleMapper.insertRoleDept(role);
        }
    }

    @Override
    public void saveAssignUser(SysRole role) {
        sysRoleMapper.saveAssignUser(role);
    }

    @Override
    public void deleteRoleUser(SysRole role) {
        sysRoleMapper.deleteRoleUser(role);
    }
}
