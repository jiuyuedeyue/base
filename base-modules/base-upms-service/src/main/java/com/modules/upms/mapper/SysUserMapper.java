package com.modules.upms.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.common.mapper.BaseMapper;
import com.modules.upms.annotation.DataScopeAop;
import com.modules.upms.entity.SysDept;
import com.modules.upms.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 通过用户名查询用户信息（含有角色信息）
     *
     * @param loginName 用户名
     * @return userVo
     */
    SysUser selectUserByLoginName(String loginName);

    /**
     * 通过手机号查询用户信息（含有角色信息）
     *
     * @param mobile 用户名
     * @return userVo
     */
    SysUser selectUserByMobile(String mobile);

    /**
     * 通过openId查询用户信息
     *
     * @param openId openid
     * @return userVo
     */
    SysUser selectUserByOpenId(String openId);

    /**
     * 更新用户密码
     * @param sysUser
     */
    void updateUserPasswordById(SysUser sysUser);

    /**
     * 根据用户ID删除用户角色关联
     * @param id
     */
    void deleteUserRoleByUserId(Long id);

    /**
     * 保存用户角色关联
     * @param entity
     */
    void saveBatchUserRole(SysUser entity);

    /**
     * 根据角色查询用户列表
     * @param sysUser
     * @return
     */
    List<SysUser> getUsersByRoleId(SysUser sysUser);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param query 实体对象封装操作类（可以为 null）
     */
    @DataScopeAop
    @Override
    IPage<SysUser> getPage(IPage<SysUser> page, @Param("query") SysUser query);

    /**
     * 登录账号或手机号是否存在
     * @param sysUser
     * @return
     */
    int isExists(SysUser sysUser);

    /**
     * 更新状态
     * @param sysUser
     */
    void updateStatus(SysUser sysUser);
}