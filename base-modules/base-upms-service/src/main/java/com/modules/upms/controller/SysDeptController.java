package com.modules.upms.controller;

import com.common.enums.CommonEnum;
import com.common.util.R;
import com.common.web.BaseController;
import com.modules.upms.common.util.TreeUtil;
import com.modules.upms.entity.SysDept;
import com.modules.upms.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门管理 前端控制器
 * </p>
 */
@RestController
@RequestMapping(value = "/sys/dept")
public class SysDeptController extends BaseController {

    @Autowired
    private SysDeptService sysDeptService;

    /**
     * 返回部门列表
     *
     * @return 树形菜单
     */
    @GetMapping(value = "/list")
    public R<List<SysDept>> getList(SysDept query) {
        List<SysDept> deptList = sysDeptService.getList(query);
        return R.success(TreeUtil.bulid(deptList, CommonEnum.TREE_ROOT_NODE.getCode()));
    }

    /**
     * 保存
     *
     * @param sysDept 实体
     * @return success/false
     */
    @PostMapping(value = "/save")
    public R<Boolean> save(@RequestBody SysDept sysDept) {
        return R.success(sysDeptService.saveOrUpdate(sysDept));
    }

    /**
     * 更新机构排序
     * @param list
     * @return
     */
    @PostMapping(value = "/updateSorts")
    public R updateSorts(@RequestBody List<SysDept> list){
        sysDeptService.updateSorts(list);
        return R.success();
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping(value = "/{id}")
    public R<Boolean> delete(@PathVariable Long id) {
        return R.success(sysDeptService.deleteById(id));
    }
}
